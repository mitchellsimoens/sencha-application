module.exports = {
    get Application () {
        return require('./Application');
    },

    get Controller () {
        return require('./Controller');
    }
};
